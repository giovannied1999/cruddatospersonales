@extends('layouts.app')

@section('content')

<form action="{{route("datos.update",$dato->id)}}" method="POST">
{{method_field('PATCH')}}
@csrf
<div class="container">
    <div class="form-group">
        <input type="text" class="form-control" name="nombre" value="{{$dato->nombre}}">
    </div>
    <div class="form-group">
        <input type="text" class="form-control" name="apellidopaterno" value="{{$dato->apellidopaterno}}" >
    </div>
    <div class="form-group">
        <input type="text" class="form-control" name="apellidomaterno" value="{{$dato->apellidomaterno}}" >
    </div>
    <div class="form-group">
        <input type="date" class="form-control" name="fechadenacimiento" value="{{$dato->fechadenacimiento}}">
    </div>
        <button type="submit" class="btn btn-primary">Guardar</button>
</div>
</form>


@endsection